# Backend demo
_by Tavo Annus_

**Frontend can be found in separate [repo](https://gitlab.com/kilpkonn/frontend-demo)**

## How to run locally
**Make sure you have:**
- Java 8 or newer
- Docker
- docker-compose

Clone repository
```bash
git clone https://gitlab.com/kilpkonn/backend-demo.git && cd backend-demo
```

Make folder to persist MySQL data
```bash
mkdir mysql-data
```

Start the database container
```bash
# use -d for detatched mode
sudo docker-compose up
```
Run spring app
```bash
./gradlew bootRun
```