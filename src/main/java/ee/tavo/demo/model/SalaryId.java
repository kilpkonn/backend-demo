package ee.tavo.demo.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
public class SalaryId implements Serializable {

    @Id
    @Column(name = "emp_no")
    private Long employeeNumber;

    @Id
    @Column(name = "from_date")
    private Date fromDate;

}
