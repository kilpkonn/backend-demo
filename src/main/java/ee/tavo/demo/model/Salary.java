package ee.tavo.demo.model;

import ee.tavo.demo.pojo.SalaryDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;


@Data
@NoArgsConstructor
@Entity
@IdClass(SalaryId.class)
@Table(schema = "demo", name = "salaries")
public class Salary {

    @Id
    @Column(name = "emp_no")
    private Long employeeNumber;

    @Column(name = "salary")
    private Long salary;

    @Id
    @Column(name = "from_date")
    private Date fromDate;

    @Column(name = "to_date")
    private Date toDate;

    public Salary(SalaryDto salaryDto) {
        employeeNumber = salaryDto.getEmployeeNumber();
        salary = salaryDto.getSalary();
        fromDate = salaryDto.getFromDate();
        toDate = salaryDto.getToDate();
    }
}
