package ee.tavo.demo.model;

import ee.tavo.demo.pojo.EmployeeDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
@Table(schema = "demo", name = "employees")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "emp_no")
    private Long employeeNumber;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender")
    private Gender gender;

    @Column(name = "hire_date")
    private Date hireDate;

    public Employee(EmployeeDto employeeDto) {
        employeeNumber = employeeDto.getEmployeeNumber();
        birthDate = employeeDto.getBirthDate();
        firstName = employeeDto.getFirstName();
        lastName = employeeDto.getLastName();
        gender = employeeDto.getGender();
        hireDate = employeeDto.getHireDate();
    }
}
