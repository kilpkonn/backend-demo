package ee.tavo.demo.repository;

import ee.tavo.demo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(value = "SELECT * FROM employees e WHERE e.emp_no = :employeeNumber", nativeQuery = true)
    Employee getEmployee(@Param("employeeNumber") Long employeeNumber);
}
