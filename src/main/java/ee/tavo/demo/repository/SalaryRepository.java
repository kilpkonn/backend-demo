package ee.tavo.demo.repository;

import ee.tavo.demo.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalaryRepository extends JpaRepository<Salary, Long> {
    @Query(value = "SELECT * FROM salaries s WHERE s.emp_no = :employeeNumber", nativeQuery = true)
    List<Salary> findByEmployee(@Param("employeeNumber") Long employeeNumber);
}
