package ee.tavo.demo.service;

import ee.tavo.demo.model.Employee;
import ee.tavo.demo.pojo.EmployeeDto;
import ee.tavo.demo.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final EmployeeRepository repository;

    public EmployeeService(EmployeeRepository repository) {
        this.repository = repository;
    }

    public List<EmployeeDto> getAllEmployees() {
        return repository.findAll().stream().map(EmployeeDto::new).collect(Collectors.toList());
    }

    public EmployeeDto getEmployee(Long employeeNumber) {
        return new EmployeeDto(repository.getEmployee(employeeNumber));
    }

    public EmployeeDto addEmployee(EmployeeDto employee) {
        return new EmployeeDto(repository.save(new Employee(employee)));
    }

}
