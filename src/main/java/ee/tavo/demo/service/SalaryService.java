package ee.tavo.demo.service;

import ee.tavo.demo.pojo.SalaryDto;
import ee.tavo.demo.repository.SalaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalaryService {

    private final SalaryRepository repository;

    public SalaryService(SalaryRepository repository) {
        this.repository = repository;
    }

    public List<SalaryDto> getSalariesForEmployee(Long employee) {
        return repository.findByEmployee(employee).stream()
                .map(SalaryDto::new)
                .collect(Collectors.toList());
    }
}
