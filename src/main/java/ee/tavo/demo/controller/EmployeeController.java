package ee.tavo.demo.controller;

import ee.tavo.demo.pojo.EmployeeDto;
import ee.tavo.demo.service.EmployeeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    @Resource
    private EmployeeService employeeService;

    @GetMapping
    public List<EmployeeDto> employees() {
        return employeeService.getAllEmployees();
    }

    @GetMapping("/{employeeNumber}")
    public EmployeeDto getEmployee(@PathVariable Long employeeNumber) {
        return employeeService.getEmployee(employeeNumber);
    }
}
