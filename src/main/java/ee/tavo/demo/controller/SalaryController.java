package ee.tavo.demo.controller;

import ee.tavo.demo.pojo.SalaryDto;
import ee.tavo.demo.service.SalaryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("salaries")
public class SalaryController {

    @Resource
    private SalaryService salaryService;

    @GetMapping("/{employeeNumber}")
    public List<SalaryDto> getSalariesForEmployee(@PathVariable Long employeeNumber) {
        return salaryService.getSalariesForEmployee(employeeNumber);
    }
}
