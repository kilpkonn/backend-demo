package ee.tavo.demo.pojo;

import ee.tavo.demo.model.Salary;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SalaryDto {
    private Long employeeNumber;
    private Long salary;
    private Date fromDate;
    private Date toDate;

    public SalaryDto(Salary salaryModel) {
        employeeNumber = salaryModel.getEmployeeNumber();
        salary = salaryModel.getSalary();
        fromDate = salaryModel.getFromDate();
        toDate = salaryModel.getToDate();
    }
}
