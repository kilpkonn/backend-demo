package ee.tavo.demo.pojo;

import ee.tavo.demo.model.Employee;
import ee.tavo.demo.model.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeDto {

    private Long employeeNumber;
    private Date birthDate;
    private String firstName;
    private String lastName;
    private Gender gender;
    private Date hireDate;

    public EmployeeDto(Employee employee) {
        employeeNumber = employee.getEmployeeNumber();
        birthDate = employee.getBirthDate();
        firstName = employee.getFirstName();
        lastName = employee.getLastName();
        gender = employee.getGender();
        hireDate = employee.getHireDate();
    }
}
